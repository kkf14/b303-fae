function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if (letter.length !== 1) {
        // If letter is invalid, return undefined.
        return undefined;
    }

    // Converts to lowercase for case sensitivity
    letter = letter.toLowerCase();
    sentence = sentence.toLowerCase();

    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    for(const char of sentence) {
        if (char === letter) {
            result++;
        }
    }

    return result;
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.

    // The function should disregard text casing before doing anything else.
    text = text.toLowerCase();

    // If the function finds a repeating letter, return false. Otherwise, return true.
     for (let i = 0; i < text.length; i++) {
        for (let j = i + 1; j < text.length; j++) {
            if (text[i] === text[j]) {
                return false; 
            }
        }
    }

    return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    if (age < 13) {
        return undefined;
    }

    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    if ((age >= 13 && age <= 21) || (age >= 65)) {
        const discountedPrice = price - (price * 0.2);
        return discountedPrice.toFixed(2).toString();
    }

    // Return the rounded off price for people aged 22 to 64.
    if (age >= 22 && age <= 64) {
        return price.toFixed(2).toString(); 
    }

    // The returned value should be a string.
}


function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    const hotCategories = {};

    for (const item of items) {
        if (item.stocks === 0) {
            hotCategories[item.category] = true;
        }
    }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    const uniqueHotCategories = Object.keys(hotCategories);

    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    return uniqueHotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    const voters = []

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    for(let i = 0; i < candidateA.length; i++) {
        for (let j = 0; j < candidateB.length; j++) {
            if(candidateA[i] === candidateB[j])
                voters.push(candidateA[i]);
        }
    }

    // Kenji Comment: This solution is inefficient because there will be additional nested loops for each additional voters.

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    return voters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};