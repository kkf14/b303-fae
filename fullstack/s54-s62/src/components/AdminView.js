import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import EditCourse from './EditCourse.js';
import ArchiveCourse from './ArchiveCourse.js';

export default function AdminView({ coursesData, fetchData }) {
	
	const [courses, setCourses] = useState([]); // State to store the courses data

  useEffect(() => {
  	const courseArr = coursesData.map(course => {
  		return (
  			<tr key={course._id}>
				  <td>{course._id}</td>
				  <td>{course.name}</td>
				  <td>{course.description}</td>
				  <td>{course.price}</td>
				  <td className={`${course.isActive ? 'text-success' : 'text-danger'}`}>
          {course.isActive ? 'Available' : 'Unavailable'}
        	</td>
				  <td><EditCourse course={course._id} fetchData={fetchData}></EditCourse></td>
          <td><ArchiveCourse course={course._id} courseStatus={course.isActive} fetchData={fetchData}></ArchiveCourse></td>
				</tr>
  		)
  })
    
    // Set the courses state when coursesData changes
    setCourses(courseArr);
  }, [coursesData]);

  return (
    <Container className="mt-5">
      <Row className="justify-content-center">
        <Col xs={12} className="text-center">
          <h1 className="mb-4">Admin Dashboard</h1>
        </Col>
        <Col xs={12}>
          <Table className="table-striped table-bordered table-hover table-responsive">
            <thead className="text-center">
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Availability</th>
                <th colSpan="2">Actions</th>
              </tr>
            </thead>
            <tbody>{courses}</tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}