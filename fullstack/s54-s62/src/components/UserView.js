import React, { useState, useEffect } from 'react';
import PriceRangeSearch from '../components/PriceRangeSearch.js';
import CourseSearch from '../components/CourseSearch.js';
import CourseCard from '../components/CourseCard.js';

export default function UserView({ coursesData }) {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    const courseArr = coursesData.map(course => {
      if(course.isActive === true){
        return (
          <CourseCard key={course._id} course={course} />
        )
      } else {
          return null;
      }
    })

    // Update userCourses when coursesData changes
    setCourses(courseArr);
  }, [coursesData]);

  return (
    <>
      <h1 className="text-center">Courses</h1>
      <PriceRangeSearch />
      <CourseSearch />
      { courses }
    </>
  );
}