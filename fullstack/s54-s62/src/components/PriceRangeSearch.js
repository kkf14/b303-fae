import React, { useState } from 'react';

const PriceRangeSearch = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [courses, setCourses] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ minPrice, maxPrice }),
      });

      if (!response.ok) {
        throw new Error('Error searching for courses');
      }

      const data = await response.json();
      setCourses(data.courses);
    } catch (error) {
      console.error('Error searching for courses:', error);
    }
  };

  return (
    <div className="container mt-5">
      <h2>Search Courses by Price Range</h2>
      <div >
        <div className="col-md-12 mb-3">
          <label htmlFor="minPrice">Min Price:</label>
          <input
            type="number"
            id="minPrice"
            className="form-control"
            value={minPrice}
            onChange={(e) => setMinPrice(e.target.value)}
          />
        </div>
        <div className="col-md-12 mb-3">
          <label htmlFor="maxPrice">Max Price:</label>
          <input
            type="number"
            id="maxPrice"
            className="form-control"
            value={maxPrice}
            onChange={(e) => setMaxPrice(e.target.value)}
          />
        </div>
        <div className="col-12">
          <button className="btn btn-primary" onClick={handleSearch}>
            Search
          </button>
        </div>
      </div>
      <h3 className="mt-4">Search Results:</h3>
      <ul>
        {courses.map((course) => (
          <li key={course._id}>{course.name}</li>
        ))}
      </ul>
    </div>
  );
};

export default PriceRangeSearch;
