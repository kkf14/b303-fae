import React from 'react';
import {Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner(props) {
	
	//const {title, content, destination, label} = data;

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{props.title}</h1>
		        <p>{props.subtitle}</p>
		        {props.buttonText === "Back home" ? (
			        <Link to="/">
			        	<Button variant="primary">Back home</Button>
			        </Link>
		        ) : (
		          	props.buttonText === "Enroll Now!" ? (
			            <Link to="/courses">
			            	<Button variant="primary">Enroll Now!</Button>
			            </Link>
		        	) : (
		            	<Button variant="primary">{props.buttonText}</Button>
		            )	
		        )}
			</Col>	
		</Row>
	)
}