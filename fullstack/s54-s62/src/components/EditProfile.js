import { useState } from 'react';
import { Button, Form } from 'react-bootstrap';

export default function EditProfile({ details, onUpdate }) {
	const [formData, setFormData] = useState({
		firstName: details.firstName,
		lastName: details.lastName,
		mobileNo: details.mobileNo,
	});

	const handleInputChange = (event) => {
		const { name, value } = event.target;
		console.log('Name:', name);
		console.log('Value:', value);

	
		// For other input fields, update the state directly
			setFormData((prevData) => ({
				...prevData,
				[name]: value,
			}));
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		try {
			const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
				body: JSON.stringify(formData),
			});

			if (response.ok) {
				const updatedUser = await response.json();
				onUpdate(updatedUser);
			}
		} catch (error) {
		  console.error(error);
		}
	};

	return (
	<>
		<h2>Update Profile</h2>
		<Form onSubmit={handleSubmit}>
			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					name="firstName"
					value={formData.firstName}
					onChange={handleInputChange}
				/>
			</Form.Group>
			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					name="lastName"
					value={formData.lastName}
					onChange={handleInputChange}
				/>
			</Form.Group>
			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile No</Form.Label>
				<Form.Control
					type="number"
					name="mobileNo"
					value={formData.mobileNo}
					onChange={handleInputChange}
				/>
			</Form.Group>
			<Button variant="primary" type="submit">
				Update Profile
			</Button>
		</Form>
	</>
	);
}