import Banner from "../components/Banner.js";
import FeaturedCourses from '../components/FeaturedCourses.js';
import Highlights from "../components/Highlights.js";

export default function Home() {
  

  return (
    <>
      <Banner
          title="Zuitt Coding Bootcamp"
          subtitle="Fish for everyone!"
          buttonText="Enroll Now!"
      />
      <FeaturedCourses />
      <Highlights />
    </>
  );
}
