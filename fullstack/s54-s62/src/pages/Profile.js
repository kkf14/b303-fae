import {useContext, useState, useEffect} from 'react';
import {Row, Col} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import EditProfile from '../components/EditProfile';
import ResetPassword from '../components/ResetPassword';
import UserContext from '../UserContext';

export default function Profile(){

    const {user} = useContext(UserContext);

    const [details,setDetails] = useState({})

    useEffect(()=>{

        fetch(`http://localhost:4000/users/details`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            // Set the user states values with the user details upon successful login.
            if (typeof data._id !== "undefined") {

                setDetails(data);

            }
        });

    },[])

    const handleProfileUpdate = (updatedUser) => {
        setDetails(updatedUser);
    };

    return (
        (user.id === null) ?
        <Navigate to="/" />
        :
        <>
            <Row>
                <Col className="p-5 bg-primary text-white">
                    <h1 className="my-5 ">Profile</h1>
                    {/* <h2 className="mt-3">James Dela Cruz</h2> */}
                    <h2 className="mt-3">{`${details.firstName} ${details.lastName}`}</h2>
                    <hr />
                    <h4>Contacts</h4>
                    <ul>
                        {/* <li>Email: {user.email}</li> */}
                        <li>Email: {details.email}</li>
                        {/* <li>Mobile No: 09266772411</li> */}
                        <li>Mobile No: {details.mobileNo}</li>
                    </ul>
                </Col>
            </Row>
            <Row className="pt-4 mt-4">
                <Col>
                    <EditProfile details={details} onUpdate={handleProfileUpdate} />
                </Col>
            </Row>
            <Row className="pt-4 mt-4">
                <Col>
                    <ResetPassword />
                </Col>
            </Row>
        </>

    )

}
