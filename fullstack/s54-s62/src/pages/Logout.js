import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';

export default function Logout() {

	// localStorage.clear();

	const {unsetUser, setUser} = useContext(UserContext);

	// To clear the localStorage upon logout
	unsetUser();

	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		});
	}, [setUser])

	return (
		// Redirect user back to the login page
		<Navigate to="/login"/>
	)
}