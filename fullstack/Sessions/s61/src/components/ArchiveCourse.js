import { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({ course, courseStatus, fetchData }){
	
	// State Hooks

	// State of the courseId for the fetch URL method
	const [courseId, setCourseId] = useState('');


	// Form state
	const [isActive, setIsActive] = useState(false)

	

	const archiveToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${course}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then((data, error) => {
			console.log(data);

			 if (data === true) {
                Swal.fire({
                    title: isActive ? 'Course Archived' : 'Course Activated',
                    icon: 'success',
                    text: 'Course successfully enabled'
                });
                setIsActive(!isActive);
                fetchData();
            } else {
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    text: 'Please try again.'
                });
            }

		}).catch(error => {
	        console.error("Error toggling active status:", error);
	    });
	}

	const activateToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${course}/activate`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then((data, error) => {
			console.log(data);

			 if (data === true) {
                Swal.fire({
                    title: 'Course Activated',
                    icon: 'success',
                    text: 'Course successfully enabled'
                });
                setIsActive(!isActive);
                fetchData();
            } else {
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    text: 'Please try again.'
                });
            }

		}).catch(error => {
	        console.error("Error toggling active status:", error);
	    });
	}

	return(
		<>
			{courseStatus === true ? 
				<Button variant="danger" size="sm" onClick={(e) => archiveToggle(e, course.id)} type="submit">Archive</Button>
				: 
				<Button variant="success" size="sm" onClick={(e) => activateToggle(e, course.id)}>Activate</Button>
			}
		</>
	)
}