import Banner from "../components/Banner.js";
import {Button, Row, Col} from 'react-bootstrap';


export default function ErrorPage() {
  return(
    <Row>
      <Col className="p-5 text-center">
        <Banner
          title="404 - Not Found"
          subtitle="The page you are looking for cannot be found"
          buttonText="Back home"
        />
      </Col>  
    </Row>
  )
}

