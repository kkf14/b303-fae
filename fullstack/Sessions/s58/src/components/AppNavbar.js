import { useState, useContext } from 'react';
import Banner from './Banner'; // Adjust the path as needed
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom'
import UserContext from '../UserContext';

export default function AppNavbar(){

	//const [user, setUser] = useState(localStorage.getItem("token"))

	const { user } = useContext(UserContext);
	console.log(user);
	console.log(`AppNavbar ${user.access}`);

	return (
	<div>
		

		<Navbar bg="light" expand="lg">
			<Container fluid>
				
		    	<Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
		   		<Navbar.Toggle aria-controls="basic-navbar-nav" />
	   			<Navbar.Collapse id="basic-navbar-nav">
	   				<Nav className="ms-auto">
	   					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
	   					<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
	   					{(user.access !== null)  ? 
	   						<>
	   							<Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
	   							<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
	   						</>
	   					: 
   							<>
			   					<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			   					<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
   							</>
	   					}
	   				</Nav>
	   			</Navbar.Collapse> 
   		 	</Container>
	  	</Navbar>
	 </div>
	)
}