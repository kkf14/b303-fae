import { useContext, React } from 'react';
import { Button, Row, Col, Form } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Profile() {

	// Allows us to consume the User context object and it's properties to use for user validation.
    const { user, setUser } = useContext(UserContext);
    console.log(`Profile ${user.access}`);


	return (
		(user.access === null) ? 
            <Navigate to="/courses" />
        :
        <Row  className="md-12 bg-primary">
        	<Col className="p-5 text-start text-light" >
        		<h1 className="py-5" >Profile</h1>
        		<h1>James Dela Cruz</h1>
        		<hr />
        		<h4>Contacts</h4>
        		<ul>
        			<li>Email: jamesDC@mail.com</li>
        			<li>Mobile No: 09211231234</li>
        		</ul>
        	</Col>
        </Row>
	)
}