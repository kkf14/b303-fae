  import { useState, useEffect } from 'react';
  import { BrowserRouter as Router } from 'react-router-dom';
  import { Route, Routes } from 'react-router-dom';
  import AppNavbar from './components/AppNavbar.js';
  import ErrorPage from './components/ErrorPage'; 
  import AddCourse from './pages/AddCourse.js';
  import Courses from './pages/Courses.js';
  import CourseView from './pages/CourseView.js';
  import Home from './pages/Home.js';
  import Login from './pages/Login.js';
  import Logout from './pages/Logout.js';
  import Profile from './pages/Profile.js';
  import Register from './pages/Register.js';
  import {Container} from 'react-bootstrap';
  import './App.css';
  import { UserProvider } from './UserContext.js';
 

  function App() {

    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    })

    // Function for clearing localStorage upon logout
    const unsetUser = () => {
      localStorage.clear();
    }

    // Used to check if the user information is properly stored upon login and that the localStorage is cleared upon logout.
    useEffect(() => {
      //console.log(user);
      //console.log(localStorage);
    }, [user])

    // To implement hot reload effeciently.
    useEffect(() => {
      fetch('http://localhost:4000/users/details', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}` 
        }
      })
      .then(res => res.json())
      .then(data => {
        //console.log(data);

          if(typeof data._id !== "undefined") {
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });
          } else {
            setUser({
              id: null,
              isAdmin: null
            })
          }
      })
    }, []);


    return (
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
            <Container> 
                <AppNavbar/>
                <Routes>
                    <Route path="/" element={<Home />}/>
                    <Route path="/addCourse" element={<AddCourse />}/>
                    <Route path="/courses" element={<Courses />}/>
                    <Route path="/courses/:courseId" element={<CourseView />}/>
                    <Route path="/register" element={<Register />}/>
                    <Route path="/login" element={<Login />}/>
                    <Route path="/logout" element={<Logout />}/>
                    <Route path="/profile" element={<Profile />}/>
                    <Route path="*" element={<ErrorPage />} />
                </Routes>
            </Container>
        </Router> 
      </UserProvider>
    );
  };

  export default App;
