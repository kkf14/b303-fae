import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useParams, useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function AddCourse(){

	// Allows us to consume the User context object and it's properties to use for user validation.
    const { user } = useContext(UserContext);
    const { courseId } = useParams();
	const navigate = useNavigate();
	
    // States needed to add a course
    const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(false);

	function createCourse(event) {
		//Prevents page load upon form submission
		event.preventDefault();

		fetch ('http://localhost:4000/courses/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				courseId: courseId
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(localStorage.getItem('token'));
			console.log (result);

			if(result) {
				setName("")
				setDescription("")
				setPrice("")
				
				Swal.fire({
				    title: "Course Added",
				    icon: "success",
				    text: "Course created succesfully!"
				})

				navigate("/courses");
			} else {
				Swal.fire({
				    title: "Unsuccessful Course Creation",
				    icon: "error",
				    text: "Course creation failed."
				})
			}
		}) 
	}

	useEffect(() => {
		// Insert effect here	
		if (
			name !== "" && 
			description !== "" && 
			price !== ""
			) 
		{
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, 
	[name, description, price]);

	return(
	(user.isAdmin !== true) ? 
        <Navigate to="/courses" />
    :
		<Form onSubmit = {(event) => createCourse(event)}>
	        <h1 className="my-5 text-center">Add Course</h1>
	            <Form.Group>
	                <Form.Label>Name:</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter Name" 
		                required
		                value = {name}
			            onChange = {event => {setName(event.target.value)}}
	                />
	            </Form.Group>
	            <Form.Group>
	                <Form.Label>Description:</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter Description" 
		                required
		                value = {description}
			            onChange = {event => {setDescription(event.target.value)}}
	                />
	            </Form.Group>
	            <Form.Group>
	                <Form.Label>Price:</Form.Label>
	                <Form.Control 
		                type="number" 
		                placeholder="Enter Price" 
		                required
		                value = {price}
			            onChange = {event => {setPrice(event.target.value)}}
	                />
	            </Form.Group>
	            

	            <Button className="my-3" variant="primary" type="submit" disabled={isActive===false}>Submit</Button>          
        </Form>
	)
}