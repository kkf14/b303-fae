import { useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard.js';


/* 
// displaying courses by index
export default function Courses () {

	const courses = coursesData.map(course => {
		return (
			<CourseCard key = {} course={course}/>
			)
	})

	return(
		<>
			<h1>Courses</h1>
			<CourseCard course={coursesData[0]}/>
			<CourseCard course={coursesData[1]}/>
			<CourseCard course={coursesData[2]}/>
		</>
	)
}
*/

// displaying courses by loop
export default function Courses () {

	// State that will be used to store the courses retrieved from the database.
	const [courses, setCourses] = useState([]);

	// Retrieves the courses from the DB upon initial render of the "Courses" component
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			setCourses(data.map(course_item => {
				return (
					<CourseCard key={course_item._id} course={course_item}/>
				)
			}))
		})
	}, []);

	// Old approach
	// const courses = coursesData.map(course_item => {
	// 	return (
	// 		<CourseCard key = {course_item.id} course={course_item}/>
	// 	)
	// })

	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}