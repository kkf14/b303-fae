  import { useState, useEffect } from 'react';
  import { BrowserRouter as Router } from 'react-router-dom';
  import { Route, Routes } from 'react-router-dom';
  import AppNavbar from './components/AppNavbar.js';
  import ErrorPage from './components/ErrorPage'; // Import ErrorPage component
  import Courses from './pages/Courses.js';
  import Home from './pages/Home.js';
  import Login from './pages/Login.js';
  import Logout from './pages/Logout.js';
  import Register from './pages/Register.js';
  import {Container} from 'react-bootstrap';
  import './App.css';
  import { UserProvider } from './UserContext.js';
  import Profile from './pages/Profile.js';

  function App() {
    const [user, setUser] = useState({
      access:localStorage.getItem('token')
    })

    // Function for clearing localStorage upon logout
    const unsetUser = () => {
      localStorage.clear();
    }

    return (
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
            <Container> 
                <AppNavbar/>
                <Routes>
                    <Route path="/" element={<Home />}/>
                    <Route path="/courses" element={<Courses />}/>
                    <Route path="/register" element={<Register />}/>
                    <Route path="/login" element={<Login />}/>
                    <Route path="/logout" element={<Logout />}/>
                    <Route path="/profile" element={<Profile />}/>
                    <Route path="*" element={<ErrorPage />} />
                </Routes>
            </Container>
        </Router> 
      </UserProvider>
    );
  };

  export default App;
