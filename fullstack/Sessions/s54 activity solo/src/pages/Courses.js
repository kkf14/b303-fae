import coursesData from '../data/coursesData.js';
import CourseCard from '../components/CourseCard.js';

/* 
// displaying courses by index
export default function Courses () {

	const courses = coursesData.map(course => {
		return (
			<CourseCard key = {} course={course}/>
			)
	})

	return(
		<>
			<h1>Courses</h1>
			<CourseCard course={coursesData[0]}/>
			<CourseCard course={coursesData[1]}/>
			<CourseCard course={coursesData[2]}/>
		</>
	)
}
*/

// displaying courses by loop
export default function Courses () {

	const courses = coursesData.map(course_item => {
		return (
			<CourseCard key = {course_item.id} course={course_item}/>
			)
	})

	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}