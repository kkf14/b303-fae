import { Row, Col, Card, Button } from "react-bootstrap";

export default function CourseCard() {
  return (
    <Row>
      <Col className="md-12">
        <Card className="cardHighlight">
          <Card.Body>
            <Card.Title>
              <h5>Sample Course</h5>
            </Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>This is a sample course offering.</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>Php 40.000</Card.Text>
            <Button variant="primary">Enroll</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
