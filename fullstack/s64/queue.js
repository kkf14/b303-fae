let collection = [];

// Print the elements in the queue
function print() {
    return collection;
}

// Enqueue elements.
function enqueue(element) {
    collection[collection.length] = element;
    return collection; 
}

// Dequeue the first element
function dequeue() {
    if (collection.length > 0) {
        for (let i = 0; i < collection.length - 1; i++) {
            collection[i] = collection[i + 1];
        }
        collection.length--;
    }
    return collection;
}

// Get the front element of the queue without removing it
function front() {
    return collection[0];
}

// Get the number of elements in the queue
function size() {
    let count = 0;
    for (let i = 0; i < collection.length; i++) {
        count++;
    }
    return count;
}

// Check if the queue is empty
function isEmpty() {
    return collection.length === 0;
}



module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};