import Banner from "../components/Banner.js";
// import FeaturedCourses from '../components/FeaturedCourses.js';
// import Highlights from "../components/Highlights.js";

export default function Home() {
  
  const data = {
      title: "Anohi",
      content: "Japanese products for everybody!",
      destination: "/products",
      label: "Buy now!"
  }


  return (
    <>
      <Banner data={data}/>
    </>
  );
}
