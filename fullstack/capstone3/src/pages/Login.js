import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {


    // Allows us to consume the User context object and it's properties to use for user validation.
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);


    function authenticate(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data.access)

            if(data.access) {
                // Syntax
                // localStorage.setItem('propertyName', value);
                localStorage.setItem('token', data.access);

                // setUser({
                //     access: localStorage.getItem('token')
                // })
                retrieveUserDetails(data.access);

                // Sweet alert .fire means to launch content
                Swal.fire({
                    title: "Login succesful",
                    icon: "success",
                    text: "Welcome to Anohi Store!"
                })

            } else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }
        })
        // Clear input fields after submission
        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });
        })
    };

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }

    }, [email, password]);

    return (   

        (user.id !== null) ? 
            <Navigate to="/products" />
        : 
            <Form onSubmit={(e) => authenticate(e)} >
                <h1 className="my-5 text-center">Login</h1>
                <Form.Group controlId="userEmail" >
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                        
                    />
                </Form.Group>

                <Form.Group controlId="password" className="my-2 ">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>
                { isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn" className="my-2">
                        Submit
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" disabled className="my-2">
                        Submit
                    </Button>
                }
            </Form>  
     
    )
}