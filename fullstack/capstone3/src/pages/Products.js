import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView.js';
import UserView from '../components/UserView.js';


// Displaying courses by loop
export default function Products () {

	// Allows us to consume the User context object and it's properties to use for user validation.
	const { user } = useContext(UserContext);

	// State that will be used to store the products retrieved from the database.
	const [products, setproducts] = useState([]);

	// Retrieves the courses from the DB upon initial render of the "Courses" component
	// useEffect(() => {
	// 	fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		//console.log(data);

	// 		setCourses(data)
	// 	})
	// }, []);

	console.log(products);
	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			setproducts(data)
		})
	};


	useEffect(() => {
		fetchData();
	}, [])

	return(
		<>
			{user.isAdmin === true ? (
				<AdminView productsData={products} fetchData={fetchData} />	/* Pass productsData to AdminView */
			) : (
				<UserView productsData={products} /> /* Pass productsData to UserView */
			)}
		</>
	)
}