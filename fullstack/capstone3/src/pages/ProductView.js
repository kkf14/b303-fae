import React, { useState, useEffect, useContext } from 'react';
import { Card, Button, Form, Container, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { productId } = useParams();
  const [quantity, setQuantity] = useState(1);
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productPrice, setProductPrice] = useState(0);

  const { user } = useContext(UserContext)

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value, 10);
    setQuantity(newQuantity);
  };

  const incrementQuantity = () => {
    setQuantity(quantity + 1);
  };

  const decrementQuantity = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  const handleOrderClick = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/${productId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({ quantity }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        console.log(data.success);

        if (data === true) {
          Swal.fire({
              title:'Order Placed!',
              icon: 'success',
              text: 'Your order has been placed!'
          });
       
        } else {
          Swal.fire({
              title: 'Error!',
              icon: 'error',
              text: 'Please try again.'
          });
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((data) => {
        setProductName(data.name);
        setProductDescription(data.description);
        setProductPrice(data.price);
      });
  }, [productId]);

  return (
  <Container className="mt-5">
    <Row>
      <Col lg={{ span: 6, offset: 3 }}>
        <Card>
          <Card.Body className="text-center"> 
            <Card.Title>{productName}</Card.Title>

            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{productDescription}</Card.Text>

            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PHP{productPrice}</Card.Text>

            <Form.Group>
              <Form.Label>Quantity:</Form.Label>
              <div className="input-group d-flex justify-content-center"> {/* Modified line */}
                <span className="input-group-btn">
                  	<Button
						variant="secondary"
						onClick={decrementQuantity}
						style={{ width: '40px', height: '40px' }}
					>
						-
				   	</Button>
                </span>
                <Form.Control
                  type="text"
                  value={quantity}
                  onChange={handleQuantityChange}
                  min={1}
                  className="text-center"
                  style={{ maxWidth: '70px' }}
                  inputMode="numeric" // Add this attribute
 				  pattern="\d*" // Add this attribute
                />
                <span className="input-group-btn">
                  	<Button
						variant="secondary"
						onClick={incrementQuantity}
						style={{ width: '40px', height: '40px' }} 
					>
					  +
					</Button>
                </span>
              </div>
            </Form.Group>

            {
              user.id !== null ?
                <Button variant="primary" onClick={handleOrderClick} className="mt-3">Place Order</Button>
                :
                <Link className="btn btn-danger mt-3" to="/login">Login to Purchase</Link>
            }
          </Card.Body>
        </Card>
      </Col>
    </Row>
  </Container>
);

}

ProductView.propTypes = {
  product: PropTypes.shape({
    _id: PropTypes.string.isRequired,
  }),
};
