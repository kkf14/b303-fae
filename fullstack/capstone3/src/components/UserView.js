import React, { useState, useEffect } from 'react';
import ProductSearch from '../components/ProductSearch.js';
import ProductCard from '../components/ProductCard.js';

export default function UserView({ productsData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const productArr = productsData.map(product => {
      if(product.isActive === true){
        return (
          <ProductCard key={product._id} product={product} />
        )
      } else {
          return null;
      }
    })

    // Update userProducts when productsData changes
    setProducts(productArr);
  }, [productsData]);

  return (
    <>
      <h1 className="text-center">Products</h1>
  
      <ProductSearch />
      { products }

    
    </>
  );
}