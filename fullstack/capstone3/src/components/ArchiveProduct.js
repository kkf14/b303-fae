import { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({ product, productStatus, fetchData }){
	
	// State Hooks

	// Form state
	const [isActive, setIsActive] = useState(false)

	

	const archiveToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${product}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then((data, error) => {
			console.log(data);

			 if (data === true) {
                Swal.fire({
                    title:'Product Archived',
                    icon: 'success',
                    text: 'Product successfully enabled'
                });
                setIsActive(!isActive);
                fetchData();
            } else {
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    text: 'Please try again.'
                });
            }

		}).catch(error => {
	        console.error("Error toggling active status:", error);
	    });
	}

	const activateToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${product}/activate`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then((data, error) => {
			console.log(data);

			 if (data === true) {
                Swal.fire({
                    title: 'Product Activated',
                    icon: 'success',
                    text: 'Product successfully enabled'
                });
                setIsActive(!isActive);
                fetchData();
            } else {
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    text: 'Please try again.'
                });
            }

		}).catch(error => {
	        console.error("Error toggling active status:", error);
	    });
	}

	return(
		<>
			{productStatus === true ? 
				<Button variant="danger" size="sm" onClick={(e) => archiveToggle()} type="submit">Archive</Button>
				: 
				<Button variant="success" size="sm" onClick={(e) => activateToggle()}>Activate</Button>
			}
		</>
	)
}