import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import EditProduct from './EditProduct.js';
import ArchiveProduct from './ArchiveProduct.js';

export default function AdminView({ productsData, fetchData }) {
    
    const [products, setProducts] = useState([]); // State to store the products data

    useEffect(() => {
        const productArr = productsData.map(product => {
            return (
                <tr key={product._id}>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    <td>{product.price}</td>
                    <td className={`${product.isActive ? 'text-success' : 'text-danger'}`}>
                    {product.isActive ? 'Available' : 'Unavailable'}
                    </td>
                    <td><EditProduct product={product._id} fetchData={fetchData}></EditProduct></td>
                    <td><ArchiveProduct product={product._id} productStatus={product.isActive} fetchData={fetchData}></ArchiveProduct></td>
                </tr>
            )
        })
        
        // Set the products state when productsData changes
        setProducts(productArr);
    }, [productsData]);

  return (
    <Container className="mt-5">
      <Row className="justify-content-center">
        <Col xs={12} className="text-center">
          <h1 className="mb-4">Admin Dashboard</h1>
        </Col>
        <Col xs={12}>
          <Table className="table-striped table-bordered table-hover table-responsive">
            <thead className="text-center">
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Availability</th>
                <th colSpan="2">Actions</th>
              </tr>
            </thead>
            <tbody>{products}</tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}